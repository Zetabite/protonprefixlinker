# ProtonPrefixLinker

ProtonPrefixLinker is a in Python written tool. It helps to create symbolic links for proton prefixes in the current folder with the name of the game.

Supports any steam libary folder now!

ProtonPrefixLinker comes in two versions:
1. single file script
2. multiple file script (for developing and stuff)

Windows isn't and will not be supported, due to the nature of proton.
If you don't know what proton is, ProtonDB has a great explanation about it on their [website](https://www.protondb.com/).