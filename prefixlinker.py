#!/usr/bin/env python
from os.path import isdir, islink, exists, realpath, dirname
from os import symlink, unlink
import steamutil
import os

if os.name == "nt":
    print("Program does not support Windows!")
    exit()

current_path = dirname(realpath(__file__))


def createSymlink(library):
    acfandprefix = steamutil.getACFandPrefix(library)
    current_path = dirname(realpath(__file__))
    for f in acfandprefix:
        dst = current_path + "/" + steamutil.getGameName(f[0])
        src = f[1]
        if exists(dst):
            if isdir(dst):
                if islink(dst):
                    print("Symlink exist, unlinking")
                    unlink(dst)
                    try:
                        symlink(src, dst)
                        print("Created symlink successfully")
                    except Exception:
                        print("Could not create symlink!")
                else:
                    print("Cannot create symlink.")
                    print("File or Folder with this name already exists!")
        else:
            print("Symlink does not exist, creating...")
            try:
                symlink(src, dst)
                print("Created symlink successfully")
            except Exception:
                print("Could not create symlink!")


def main():
    libraries = steamutil.getSteamLibraries()
    for library in libraries:
        createSymlink(library)
    return


if __name__ == '__main__':
    main()
