from os.path import expanduser, isfile, isdir, join, islink
from os import listdir

# FOLDERS
homedir = expanduser("~")
steamdir = homedir + "/.local/share/Steam/"

# Steams default library dir
stdlibrarydir = steamdir + "steamapps/"

# compatdatadir = stdlibrarydir + "compatdata/"
# commondir = stdlibrarydir + "common/"
libraryvdf = stdlibrarydir + "libraryfolders.vdf"


def lineContainsLibrary(line):
    matchers = ["LibraryFolders", "TimeNextStatsReport", "ContentStatsID", '{', '}']
    for matcher in matchers:
        if matcher in line:
            return False
        else:
            continue
    else:
        return True


def getSteamLibraries():
    libraries = [stdlibrarydir]
    output = open(libraryvdf, 'r')
    for line in output:
        if lineContainsLibrary(line):
            qmcount = 0
            library = ""
            for c in line:
                if c == '"':
                    qmcount += 1
                    if qmcount == 4:
                        libraries.append(library + "/steamapps/")
                        break
                elif qmcount == 3:
                    library += c
        else:
            continue
    output.close()
    return libraries


# Getting the proton prefixes in other words the game steamid
def getPrefixes(library):
    compatdatadir = library + "compatdata/"
    prefixes = [d for d in listdir(compatdatadir) if isdir(join(compatdatadir, d))]
    return prefixes


def getACFandPrefix(library):
    prefixes = getPrefixes(library)
    compatdatadir = library + "compatdata/"
    acf_prefix = []
    for f in listdir(library):
        if isfile(join(library, f)) and f.lower().endswith(".acf"):
            for prefix in prefixes:
                if f[12:12 + len(prefix)] == prefix:
                    acf_file = library + f
                    gamefolder = getGameFolder(library, acf_file)
                    if hasExecutable(gamefolder):
                        acf_prefix.append([acf_file, compatdatadir + prefix])
    return acf_prefix


def hasExecutable(folder):
    for e in listdir(folder):
        if not islink(join(folder, e)):
            if isdir(join(folder, e)):
                if hasExecutable(folder + "/" + e):
                    return True
            if isfile(join(folder, e)) and e.lower().endswith(".exe"):
                return True
    return False


def getGameFolder(library, acf_file):
    return library + "common/" + getGameName(acf_file) + "/"


def getGameName(acf_file):
    output = open(acf_file, 'r')
    for line in output:
        if "installdir" in line:
            qmcount = 0
            installdir = ""
            for c in line:
                if c == '"':
                    qmcount += 1
                    if qmcount == 4:
                        output.close()
                        return installdir
                elif qmcount == 3:
                    installdir += c
